import { useState } from 'react'
import { Route, Routes, Navigate } from "react-router-dom";
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Login from './views/Login';
import InputQuestion from './views/InputQuestion';
import SignUp from './views/SignUp';
import Header from './views/Header';
import useAuth from './hooks/AuthProvider';
import Menu from './views/Menu';

function App() {

  const {user} = useAuth()

  // console.log({user})
  if(user){  
  return (
    <Header>
    <Routes>
      <Route exact path='/mainmenu' element={<Menu />} />
      <Route exact path='/inputQuestion' element={<InputQuestion />} />
      <Route exact path='/login'element={<Navigate to='/mainmenu' />} />
    </Routes>
  </Header>
  )}else{
    return (
      <>
      <Routes>
      <Route exact path='/' element={<Navigate to='/login' />} />
      <Route path="*" element={<Navigate to='/login' />} />
        <Route exact path='/login' element={<Login />} />
      <Route exact path='/signup' element={<SignUp />} />
      </Routes>
    </>
    )
  }
}

export default App
