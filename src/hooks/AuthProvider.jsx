import React, { useEffect, useState } from 'react'
import LogIn from '../models/User/LogIn'

const AuthContext = React.createContext();

export function AuthProvider({children}) {
    const {inputData, result, loading, error} = LogIn()
    const [user, setUser] = useState([])

  
      const handleUserdata = async () =>{

        try{
          const result = JSON.parse(sessionStorage.getItem('user'))
          setUser(result)

        }catch(e){
          console.log('e jalan', e)
          setUser(null)
          sessionStorage.clear()
        }

      }

      

    useEffect(() => {
      handleUserdata()
      if(result?.response == 200){
        setUser(result?.result)
        sessionStorage.setItem('user', JSON.stringify(result?.result))
      }
    }, [result])

    const signIn = async (payload) => {
      try{
        
        inputData(payload)
      }catch(e){
        console.log(e)
      }
    }

    const signOut = () => {
      setUser(null)
      localStorage.clear()
      sessionStorage.clear()
    };

    const memoedValue = React.useMemo(
      () => ({
        user,
        setUser,
        signIn,
        signOut
      }),
      [user]
    );
    


    return (
      <AuthContext.Provider value={memoedValue}>
        {children}
      </AuthContext.Provider>
    );
}


export default function useAuth() {
  return React.useContext(AuthContext);
}
