import React, { useState, useEffect } from 'react';

const Timer = ({startTime, element, modalStatus}) => {
  const [time, setTime] = useState(startTime); 


  useEffect(() => {
    const timer = setInterval(() => {
        setTime(prevTime => prevTime - 1); 
    }, 1000);
    
    if(time < 1){
        clearInterval(timer)
    }
    
    return () => {
        clearInterval(timer);
    };
}, []);


useEffect(() => {
    
    localStorage.setItem('leftTime', time)
    if(time < 1) {
        element?.current?.click()
    }
  }, [time])

  // Format waktu menjadi menit:detik
  const formatTime = () => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return time < 1 ? '0:00' : `${minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
  };

  return (
    <div>
      <p>{formatTime()}</p>
    </div>
  );
};

export default React.memo(Timer);
