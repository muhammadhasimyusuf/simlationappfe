import React, { useEffect, useRef, useState } from 'react';
import Modal from './componen/Modal';
import QuestionGet from '../models/Question/QuestionGet';
import Timer from './componen/Timer';
import useAuth from '../hooks/AuthProvider';
import qAnswer from '../models/Question/qAnswer';
import GetAnswer from '../models/Question/GetAnswer';
import FinishTest from '../models/User/FinishTest';
import ResetTest from '../models/Question/ResetTest';

function Menu() {
  const [showModal, setShowModal] = useState(false);
  const [showModalHistory, setShowModalHistory] = useState(false);
  const {Question, result, reQuestion} = QuestionGet()
  const ref = useRef(null);
  const [ans, setAns] = useState('')
  const [qlist, setQlist] = useState('')
  const [qsNo, setqsNo] = useState(1)
  const {user, setUser} = useAuth()
  const {inputData} = qAnswer()
  const {getAnswer, getScore, score, resultAns, } = GetAnswer()
  const {Finish, resultFinish} = FinishTest()
  const {resultreset, reset} = ResetTest()

  const [initpage, setInitpage] = useState({
    from : 1,
    to : 10,
})

const handlepage = (e, type) => {
  if(type == 'next'){
    setInitpage(prev => {
      return {
        to : prev.to + 10,
        from : prev.from + 10
      }
    }
      )
  }
  if(type == 'prev'){
    setInitpage(prev => {
      return {
        to : prev.to - 10,
        from : prev.from - 10
      }
    }
      )
  }
}

useEffect(() => {
  if(resultFinish?.response == 200){
    setUser(resultFinish?.result)
    sessionStorage.setItem('user', JSON.stringify(resultFinish?.result))
  }
}, [resultFinish])

useEffect(() => {
  if(resultreset?.response == 200){
    setUser(resultreset?.result)
    sessionStorage.setItem('user', JSON.stringify(resultreset?.result))
  }
}, [resultreset])

  


  const leftTime = localStorage.getItem('leftTime')

  useEffect(() => {
    setAns(resultAns?.result?.qanswer)
  
  return(()=>{
    setAns('')
  })
  }, [resultAns?.result])

  useEffect(()=>{
   showModal && getAnswer(user.nim, result?.result?.data[0]?.no)
  }, [result?.result])

  // useEffect(() => {
  // if(score != ''){  if(score?.status == "ERROR"){
  //     alert(`${score?.result}`)
  //   }else{
  //     alert(`task selesai, score anda ${score?.result?.scores}`)}
  //   }
  // }, [score])
  

  const handleModal = (e, type, data) => {
    e.preventDefault()
    e.stopPropagation()

    if(type == 'start_test'){
      if(user?.isFinish == 1){
        return alert('Anda Telah menyelesaikan tes')
      }
      setShowModal(true)
      Question(user.qtype)
    }
    if(type == 'finishTest'){
      getScore(user.nim)
      setShowModal(false)
      Finish(user.nim)
    }
    if(type == 'reset'){
      
      if(confirm('reset test?') == true){
        reset(user.nim)
      }
    }
    if(type == 'history'){
      if(user?.isFinish == 0){
        return alert('Anda belum menyelesaikan tes')
      }
      setInitpage(
        {
          from : 1,
          to : 10,
        }
      )
      getScore(user.nim)
      setShowModalHistory(true)
    }
    if(type == 'docDownload'){
      fetch('PM_14_TAHUN_2019.pdf')
      .then(response => {response.blob()
      .then(blob => {
            // Creating new object of PDF file
            const fileURL = window.URL.createObjectURL(blob);
            // Setting various property values
            let alink = document.createElement('a');
            alink.href = fileURL;
            alink.download = 'PM_14_TAHUN_2019.pdf';
            alink.click();
        })
    })
    }
    if(type == 'wDownload'){
      fetch('kisi.docx')
      .then(response => {response.blob()
      .then(blob => {
            // Creating new object of PDF file
            const fileURL = window.URL.createObjectURL(blob);
            // Setting various property values
            let alink = document.createElement('a');
            alink.href = fileURL;
            alink.download = 'kisi.docx';
            alink.click();
        })
    })
    }


  }

  const handleRadio = (e, no) => {
    setQlist(e.target.getAttribute('data'))
    setAns(e.target.value)
    const data = {
      qno  : no,
      nim : user.nim,
      qlist : e.target.getAttribute('data'),
      qanswer :e.target.value,
    }
    e.preventDefault();
    e.stopPropagation();
      inputData(data)
    
  }

  const handleQuestion = (e, qno, qlist, qanswer) => {
    const data = {
      qno  : qno,
      nim : user.nim,
      qlist : qlist,
      qanswer : qanswer,
    }
    e.preventDefault();
    e.stopPropagation();
      inputData(data)
  }

 
  

  return (
    
    <main>
    <div className="px-4 bg-blue-400 min-h-screen sm:px-6 lg:px-8 py-8 w-full pt-60 max-w-9xl mx-auto">
      <div className="flex flex-col justify-center items-center mb-4">
        <h1 className="text-4xl text-white font-bold mb-10">Personal Licence Exam Simulation</h1>
        {/* <p className="text-gray-700">Please enter your credentials to log in.</p> */}
      </div>
      <center>
      <div className="rounded-lg  max-w-2xl mx-auto">
        {/* <form onSubmit={handleSubmit}> */}
          <div className=''>
           <span className='text-xs'>Start Test</span>
          <div className="mb-3 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <button
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600 leading-tight focus:outline-none focus:shadow-outline"
              onClick={(e) =>  handleModal(e, "start_test")}
            >
              Start Test
            </button>
          </div>
          <div className="mb-3 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <button
            onClick={e => handleModal(e, 'history')}
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600 leading-tight focus:outline-none focus:shadow-outline"
            >
              History Point
            </button>
          </div>
          {user.isFinish == 1 &&<div className="mb-3 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <button
            onClick={e => handleModal(e, 'reset')}
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600 leading-tight focus:outline-none focus:shadow-outline"
            >
              Reset Test
            </button>
          </div>}
          <div className="mb-4 w-1/2">
          </div>

          <Modal
          showModal = {showModal}
          setShowModal = {setShowModal}
          headerModal = {'Test'}
          >
            <div className='w-full'>
            <div className='main-sec h-[220px]'>
               <div className='flex justify-start px-7'>
                  <span>{result?.result?.data[0]?.no}.</span>
                  <span className='ml-3 text-left'>{result?.result?.data[0]?.question}</span>
               </div>
               <div className='flex flex-col pl-11'>
                {
                  result?.result.data[0].listchoice.map(data => {
                    return(
                      <span key={data.qtype} className='flex justify-start'>
                        <input  type="radio" checked={ans == data?.qtype} onChange={e => handleRadio(e, result?.result?.data[0]?.no)} name={data.qno} data={data.qtype} value={data?.qtype} /><span className='ml-3 mr-2'>{data.qtype}.</span> {data.choice} 
                      </span>
                    )
                  })
                }

               </div>
               <div>
               </div>
               </div>
                <p>{result?.result?.current_page} / {result?.result?.total}</p>
               <div className='flex justify-between mt-16 px-4'>
                  <span>
                  <button 
                  onClick={() => reQuestion(result?.result?.prev_page_url)}
                  className='btn-sm rounded-lg w-full py-2 px-3 text-black hover:text-white hover:bg-gray-300 bg-gray-100 leading-tight'>
                      back
                    </button>
                  </span>
                 {showModal && <span>
                    <Timer
                     startTime={leftTime ? leftTime : 60 * 60}
                     element = {ref}
                     modalStatus =  {showModal}
                    />
                  </span>}
                  <span>
                      <button
                    onClick={() =>{ reQuestion(result?.result?.next_page_url) ; setAns('')}}
                    hidden = {result?.result?.current_page == result?.result?.total}
                    className='btn-sm rounded-lg w-full py-2 px-3 text-black hover:text-white hover:bg-gray-300 bg-gray-100 leading-tight'>
                      next
                    </button>
                    <button
                    onClick={(e) => handleModal(e, 'finishTest')} 
                    ref={ref}
                    hidden = {result?.result?.current_page != result?.result?.total}
                    className='btn-sm rounded-lg w-full py-2 px-3 text-black hover:text-white hover:bg-gray-300 bg-gray-100 leading-tight'>
                      Finish
                    </button>
                  </span>
               </div>
               </div>
          </Modal>

          <Modal
          showModal = {showModalHistory}
          setShowModal = {setShowModalHistory}
          headerModal = {'History'}
          >
            <div className='w-full'>
              <div className='h-full'>
                <span>
                  <h3>
                    Score Akhir: {score?.result?.scores}
                  </h3>
                </span>
                <div className='h-[500px]'>
                <table className='border-b-1 border-black divide-y divide-slate-700'>
                  <thead>
                    <tr>
                      <td className='px-3'>No</td>
                      <td className='px-3'>Soal</td>
                      <td className='px-3'>Jawaban</td>
                      <td className='px-3'>Benar</td>
                      <td className='px-3'>Hasil</td>
                    </tr>
                  </thead>
                  <tbody className=' divide-y divide-slate-700'>
                    {
                    score?.result?.answer?.filter(data => data.no >= initpage.from && data.no <= initpage.to).map(data=>{
                      return(
                      <tr key={data.no} className=''>
                        <td className='px-3'>{data.no}</td>
                        <td className='px-3'>{data.question}</td>
                        <td className='px-3'>{data.answer}</td>
                        <td className='px-3'>{data.true}</td>
                        <td className='px-3 '>{data.result ? "Benar" : "Salah"}</td>
                      </tr>
                      )
                      })

                      }
                  </tbody>
                </table>
                </div>
                <div>
                   <span className='flex justify-between w-1/2'>
                    <button onClick={e =>initpage.from > 2 && handlepage(e, "prev")} className='btn-sm rounded-lg w-full py-2  text-black bg-gray-100 w-12'>prev</button>
                    <button onClick={e =>initpage.from < 40 && handlepage(e, "next")} className='btn-sm rounded-lg w-full py-2  text-black bg-gray-100 w-12'>next</button>
                   </span>
                </div>
              </div>
            </div>
          </Modal>
                    <span className='text-xs'>others</span>
                    <div>
                      <div className="mb-3 w-1/2">
                        {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
                          Password
                        </label> */}
                        <button  onClick={e=> handleModal(e, 'wDownload')}
                          className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600
                          leading-tight focus:outline-none focus:shadow-outline"
                          >
                          Kisi - kisi soal
                        </button>
                      </div>
                      <div className="mb-3 w-1/2">
                        {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
                          Password
                        </label> */}
                        <button  onClick={e=> handleModal(e, 'docDownload')}
                          className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600
                          leading-tight focus:outline-none focus:shadow-outline"
                          >
                          Dokumen
                        </button>
                      </div>
                    </div>
          </div>
        {/* </form> */}
      </div>
      </center>
    </div>
    </main>
  );
}

export default Menu;
