import React, { useEffect, useRef } from 'react';
import Test from '../models/Test';
import UserCreate from '../models/User/UserCreate';
import { useNavigate } from 'react-router-dom';

function SignUp() {
  const nim = useRef();
  const name= useRef();
  const password = useRef();

const {inputData, result, error, setError} = UserCreate()

  const handleSubmit = (e) => {

      e.preventDefault()

      let rand = Math.floor(Math.random() * 10);
      let qtype
      if(rand % 2 == 0){
        qtype = 'a'
      }else{
        qtype = 'b'
      }
    const data = {
        name : name.current.value,
        nim : nim.current.value,
        password : password.current.value,
        qtype : qtype
    }
    
    inputData(data)

  }

  useEffect(() => {
    if(result?.message == "data added"){

    name.current.value = ''
    nim.current.value = ''
    password.current.value = ''

    }
  }, [result])
  

  const navigate = useNavigate()
  const handleSign = () => {

   return navigate('/login')

  }


  return (
    
    <main>
    <div className="px-4 bg-blue-400 min-h-screen sm:px-6 lg:px-8 py-8 w-full pt-60 max-w-9xl mx-auto">
      <div className="flex flex-col justify-center items-center mb-4">
        <h1 className="text-4xl text-white font-bold mb-10">Create User</h1>
        {/* <p className="text-gray-700">Please enter your credentials to log in.</p> */}
      </div>
      <center>
      <div className="rounded-lg p-8 max-w-2xl mx-auto">
        <form onSubmit={handleSubmit}>
          <div className=''>
          <div className="mb-7 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="email">
              Email
            </label> */}
            <input
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="name"
              type="name"
              placeholder="Name"
              ref={name}
              required
            />
          </div>
          <div className="mb-7 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="email">
              Email
            </label> */}
            <input
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="nim"
              type="text"
              placeholder="NIT"
              ref={nim}
              required
            />
          </div>
          <div className="mb-7 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <input
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="password"
              type="password"
              placeholder="Password"
              ref={password}
              required
            />
          </div>
          <div className="mb-3 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <input
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600 leading-tight focus:outline-none focus:shadow-outline"
            //   id="password"
              type="submit"
              value="Create"
            />
          </div>
          <div className="mb-3 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <button
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-black bg-blue-200 leading-tight focus:outline-none focus:shadow-outline"
            //   id="password"
            type='button'
            onClick={handleSign}
            >
              LogIn
            </button>
          </div>
          </div>
        </form>
      </div>
      </center>
    </div>
    </main>
  );
}

export default SignUp;
