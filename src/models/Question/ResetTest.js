import axios from 'axios';
import { useState, useEffect } from "react";


function ResetTest(){
    
    const [resultreset, setResultreset] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const reset = (id) => {
          setLoading(true);
          axios({
              method: 'POST',
              url: `${Base_URL}/delete/${id}`,
            }).then((response) => {
                setResultreset(response.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }
    
    return { reset, resultreset, loading, error,};
}

export default ResetTest