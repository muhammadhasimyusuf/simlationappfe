import axios from 'axios';
import { useState, useEffect } from "react";


function GetAnswer(){
    
    const [resultAns, setResultAns] = useState(null);
    const [score, setscore] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const getAnswer = (nim, qno) => {
        //   let formdata = new URLSearchParams();
        
          setLoading(true);
          axios({
              method: 'GET',
              url: `${Base_URL}/answered/${nim}/${qno}`,
            //   data: formdata,
            }).then((response) => {
                setResultAns(response?.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }
    const getScore = (nim) => {
        //   let formdata = new URLSearchParams();
        
          setLoading(true);
          axios({
              method: 'GET',
              url: `${Base_URL}/scores/${nim}`,
            //   data: formdata,
            }).then((response) => {
                setscore(response?.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }
    
    return { getAnswer, getScore, score, resultAns, loading, error,};
}

export default GetAnswer