import axios from 'axios';
import { useState, useEffect } from "react";


function UserCreate(){
    
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const inputData = (payload) => {
        console.log({payload})
          let formdata = new URLSearchParams();
          formdata.append("name", payload.name);
          formdata.append("nim", payload.nim);
          formdata.append("qtype", payload.qtype);
          formdata.append("password", payload.password);
          setLoading(true);
          axios({
              method: 'POST',
              url: `${Base_URL}/user/add`,
              data: formdata,
            }).then((response) => {
                setResult(response.data);
                if(response.data?.message == "data added"){
                    alert('Berhasil Terdaftarr')
                }
          }).catch((err)=>{
              setError(err);
              alert('data tidak sesuai')
          }).finally(()=>{
              setLoading(false);
          });
      }
    
    return { inputData, result, loading, error, setError};
}

export default UserCreate