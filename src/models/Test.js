import axios from 'axios';
import { useState, useEffect } from "react";


function Test(){
    
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    useEffect(() => {
        setLoading(true);
        axios({
            method: 'GET',
            url: 'http://127.0.0.1:8000/api',
            // headers: { "Access-Control-Allow-Origin": "*" },
          }).then((response) => {
            setResult(response.data);
        }).catch((err)=>{
            setError(err);
        }).finally(()=>{
            setLoading(false);
        })
    }, [])
    
    return { result, loading, error,};
}

export default Test